#!/usr/bin/python3

# Write a program that reads in 10 integers, appending each onto a list. It then displays
# the list from beginning to end using a loop, accessing each element via its index.
# Next, it displays the list from end to beginning using a loop, accessing each element
# via negative index. Finally, it displays the list from end to beginning, accessing
# the elements of the lsit without using indices. Hint: use the pop() method.

num_list = []
for i in range(10):
    num_list.append(int(input('Enter integer: ')))

print(f'Printing list members from beginning...')
for i in range(len(num_list)):
    print(f'{num_list[i]}', end=' ')
print()

print(f'Printing list members from end...')
for i in range(len(num_list)):
    print(f'{num_list[-i-1]}', end=' ')
print()

print(f'Printing list members from end using pop...')
for i in range(len(num_list)):
    print(f'{num_list.pop()}', end=' ')
print()

