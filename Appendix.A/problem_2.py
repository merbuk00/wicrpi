#!/usr/bin/python3

# Write a program that reads a text file and outputs an exact copy to another file. Access the input and
# output file names from the command line.
# Each line in the output file should be prefixed with its line number. The line number for each should
# occupy four spaces, right justified.

import sys, argparse

parser = argparse.ArgumentParser(description='Copy contents from source to destination file')
parser.add_argument('src_file', type=str, help='Source text file')
parser.add_argument('dst_file', type=str, help='Dest text file')

args = parser.parse_args()

try:
    with open(args.src_file, 'r') as f1, open(args.dst_file, 'w') as f2:
        i = 1
        for line in iter(f1.readline, ''):
            f2.write(f'{i:>4} {line}')
            i+=1
except EnvironmentError: # parent of IOError, OSError *and* WindowsError where available
    print('Error opening input file; does it exist or corrupted?')
    print('')
