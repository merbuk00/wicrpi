#!/usr/bin/python3

# Write a program that reads in a string and determines if it is a palindrome
# (i.e., a string that reads the same backwards as forwards.)

import sys, re

def is_palindrome(s):
    return s == s[::-1]

def main():
    while True:
        str_NY = f' '
        my_str = input(f'Enter string, or \'x\' to exit: ')

        # Remove punctuations in string using regex 
        sentence = (re.sub(r'[^\w\s]', '', my_str)).lower()

        if sentence == 'x':
            break

        if not is_palindrome(sentence):
            str_NY = f' not '

        print(f'String "{my_str}"" is{str_NY}a palindrome...\n')


if __name__ == "__main__":
    # execute only if run as a script
    main()
