#!/usr/bin/python3

# Write a function that is passed two lists. Your function should concatenate
# the elements of the second list onto the first list, and then return the
# modified first list. Write a program that tests your function.


def cat_list(list1, list2):
    """Concatenate two lists
    input: list1, list2
    output: concatenated list, list1 followed by list2"""

    return list1 + list2

def main():
    a = [1, 2, ['a', 'hello', 1], 3]
    b = [100, 'money', True]
    print(f'concatenated list: {cat_list(a, b)}')

if __name__ == "__main__":
    # execute only if run as a script
    main()
