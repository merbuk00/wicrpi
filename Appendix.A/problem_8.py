#!/usr/bin/python3

# WWrite a program that creates a dictionary whose key/value pairs
# are 'a'/1, 'b'/2, ..., 'z/26'. Then prompt for and read in a
# letter and display its corresponding number value obtained from
# your dictionary.

import sys

def alpha_dict():
    # dictionary comprehension
    return({chr(ord('a')+i):i+1 for i in range(26)})

def main():
    try:
        ad = alpha_dict()
        while True:
            ch = input("Enter a char from 'a' to 'z', any other char to exit: ")
            if ch >= 'a' and ch <= 'z':
                print(f'Key/value is {ch}:{ad[ch]}\n')
            else:
                raise RuntimeError("Exiting.\n")
    except RuntimeError as emsg:
        print(emsg)
        sys.exit(1)

if __name__ == "__main__":
    # execute only if run as a script
    main()
