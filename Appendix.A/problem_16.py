#!/usr/bin/python3

# Write a program that reads in a positive integer into n. Your program should
# then display n rows. Each row should have consecutive integers starting from
# 1 and have one more integer than the preceding row. The first row should
# contain only 1. For example, if 3 is entered, then your program should display
# 1
# 1 2
# 1 2 3

import sys

def main():
    while True:
        try:
            my_inp = input(f'Enter a +ve integer, \'x\' to exit: ')
            if my_inp.lower() == f'x':
                break

            my_int = int(my_inp)
            if my_int <= 0:
                raise RuntimeError("Integer must be +ve!\n")

            s = ''
            for i in range(1, my_int+1):
                s += f'{i} '
                print(f'{s}'.strip())

        except RuntimeError as emsg:
            print(emsg)
        except:
            print(f'You didn\'t enter an integer!\n')


if __name__ == "__main__":
    # execute only if run as a script
    main()
