#!/usr/bin/python3

# Write a program that reads in 10 strings from the keyboard. Count the 
# number of strings that start with 'pr'. Display this count. Use a loop.

def main():
    str_list = []
    print(f'Enter 10 strings, one on each line...')
    for i in range(10):
        pmt_str = f'Enter string {i+1}: '
        my_str = input(pmt_str)
        str_list.append(my_str)

    count_pr = 0
    for my_str in str_list:
        if my_str.startswith('pr'):
            count_pr += 1

    print(f'No of strings which start with "pr" is: {count_pr}\n')

if __name__ == "__main__":
    # execute only if run as a script
    main()
