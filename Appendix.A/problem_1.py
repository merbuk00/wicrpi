#!/usr/bin/python3

# Write a program that displays your name 10 times. Use a loop.

i = 0
while i < 10:
    print('My name is Bob.')
    i+=1
