#!/usr/bin/python3

# Write a program that reads in a text file that consists of some standard English
# text. Your program should count the number of occurrences of each letter of the
# alphabet and display each letter with its count. What are the six most frequently
# used letters?

import sys, argparse

parser = argparse.ArgumentParser(description='Count number of occurrences of letters in a text file')
parser.add_argument('src_file', type=str, help='Source text file')

args = parser.parse_args()

d = {}

def main():
    try:
        with open(args.src_file, 'r') as f:
            for ch in f.read():
                ch = ch.lower()

                if ord(ch) >= ord('a') and ord(ch) <= ord('z'):
                    d[ch] = (d.get(ch, 0) + 1)

            for ord_ch in range(ord('a'), ord('z')+1):
                print(f'Char \'{chr(ord_ch)}\': {d.get(chr(ord_ch))}')

            print()
            print(f'Six most used characters in desc order...')
            for i in range(6):
                m_key = max(d, key=d.get)
                print(f'{i+1}: Letter \'{m_key}\'')
                d.pop(m_key)

    except EnvironmentError: # parent of IOError, OSError *and* WindowsError where available
        print('Error opening input file; does it exist or corrupted?')
        print('')


if __name__ == "__main__":
    # execute only if run as a script
    main()
