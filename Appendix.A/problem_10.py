#!/usr/bin/python3

# Write a program that reads in a positive integer n. Your program should
# then display True if n is a perfect number, and False otherwise. A perfect
# number is a number whose positive divisors excluding itself sum up to the
# given number. For example, the positive divisors of 6 excluding 6 are
# 1, 2, and 3. Because 1 + 2 + 3 = 6, 6 is a perfect number.

import sys

class ExitSignal(Exception):
    pass

def get_pos_div(n):
    """Returns list of positive divisors of n
    Input: positive int n
    Output: list of positive divisors, excluding n"""

    return [i for i in range(1, n) if n%i == 0]

def main():
    while True:
        try:
            str_val = input("Enter a positive integer, or 'x' to exit: ")

            if str_val.lower() == 'x':
                raise ExitSignal(f'Exiting...\n')
            else:
                int_val = int(str_val)
                if int_val <= 0:
                    print(f'Zero or negative integers are not allowed...\n')
                    continue

                b_pNum = True if sum(get_pos_div(int_val)) == int_val else False
                print(f'Is {int_val} a perfect number? {b_pNum}\n')

        except ExitSignal as emsg:
            print(emsg)
            sys.exit(1)
        except:
            print(f'Invalid entry...\n')
            # continue

if __name__ == "__main__":
    # execute only if run as a script
    main()
