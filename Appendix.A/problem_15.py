#!/usr/bin/python3

# Read in a positive number into x. Then execute the folowing statement 100 times:
#     x = math.sqrt(x)
# Does the value of x converge on a particular number, regardless of its initial value?
# Try vlues both less than and greater than 1. To use sqrt(), import the math module.

import sys, math

def main():
    while True:
        try:
            my_inp = input(f'Enter a +ve number, \'x\' to exit: ')
            if my_inp.lower() == f'x':
                break

            my_num = float(my_inp)
            if my_num <= 0:
                raise RuntimeError("Number must be +ve!\n")

            x = my_num
            for i in range(100):
                x = math.sqrt(x)

            print(f'The square root of {my_num} over 100 iterations: {x}\n')

        except RuntimeError as emsg:
            print(emsg)
        except:
            print(f'You didn\'t enter an +ve number!\n')


if __name__ == "__main__":
    # execute only if run as a script
    main()
