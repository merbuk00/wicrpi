#!/usr/bin/python3

# Write a function that is passed two lists. Your function should concatenate
# the elements of the second list onto the first list, and then return the
# modified first list. Write a program that tests your function.


def getgrade():
    """Concatenate two lists
    input: list1, list2
    output: concatenated list, list1 followed by list2"""
    try:
        grade = int(input(f'Enter grade between 0 and 100 inclusive: '))
        if grade < 0 or grade > 100:
            raise RuntimeError("An invalid grade was entered.\n")

        print(f'You grade is in the quartile: ', end= ' ')
        if grade >= 75:
            print(f'(75-100)')
        elif grade >= 50:
            print(f'(50-74)')
        elif grade >= 25:
            print(f'(25-49)')
        else:
            print(f'(0-24)')

        print()
        return True

    except RuntimeError as emsg:
        print(emsg)
        return False
    except:
        print(f'You didn\'t enter an integer!\n')
        return False

def main():
    while getgrade() == False:
        continue

if __name__ == "__main__":
    # execute only if run as a script
    main()
