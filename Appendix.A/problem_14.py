#!/usr/bin/python3

# Write a program that reads in a positive integer into n. Your program should then
# sum up the first n positive odd numers and display the sum. What is the relation
# between the value of n and the computed sum?

import sys

def main():
    while True:
        try:
            my_inp = input(f'Enter a +ve integer, \'x\' to exit: ')
            if my_inp.lower() == f'x':
                break

            my_int = int(my_inp)
            if my_int <= 0:
                raise RuntimeError("Integer must be +ve!\n")

            print(f'Sum of 1st {my_int} odd integers: {sum([i for i in range(1, my_int*2, 2)])}\n')

        except RuntimeError as emsg:
            print(emsg)
        except:
            print(f'You didn\'t enter an integer!\n')


if __name__ == "__main__":
    # execute only if run as a script
    main()
