#!/usr/bin/python3

# Write a program that computes the following sum: 2 + 1/2! + 1/3! + ... + 1/100!
# Is the sum equal to an important constant in mathematics? "!" denotes the
# factorial function. n! is the product of the integers from 1 to n. For example,
# 5! = 1x2x3x4x5 = 120

import math

def fact_list(num):
    return [math.factorial(i) for i in range(1, num+1)]


def main():
    my_fact = fact_list(100)

    my_sum = 2
    my_sum += sum(1/my_fact[i] for i in range(1, len(my_fact)))

    print(f'Sum: {my_sum}')


if __name__ == "__main__":
    # execute only if run as a script
    main()
